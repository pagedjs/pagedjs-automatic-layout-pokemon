const pokemonContainer = document.querySelector('.pokemons');

// Function to generate random number between min (inclusive) and max (exclusive)
function getRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}


// Function to fetch random Pokémon data and populate the HTML
async function populatePokemonData() {
  for (let i = 0; i < 100; i++) {
    const randomPokemonId = getRandomNumber(1, 899); // Generate a random Pokémon ID
    
    try {
      // Fetching Pokémon data
      const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${randomPokemonId}`);
      const pokemonData = await response.json();

      // Fetching Pokédex entry data
      const speciesResponse = await fetch(`https://pokeapi.co/api/v2/pokemon-species/${randomPokemonId}`);
      const speciesData = await speciesResponse.json();

      // Get the French version of the description
      const descriptionEntry = speciesData.flavor_text_entries.find(entry => entry.language.name === 'fr');
      const description = descriptionEntry.flavor_text;

      const html = `
        <section class="pokemon" id="${pokemonData.name}">
          <h2>${pokemonData.name}</h2>
          <img src="${pokemonData.sprites.front_default}" alt="${pokemonData.name}-image" class="img">
          <p>Type: ${pokemonData.types[0].type.name}</p>
          <span class="size-weight">Size and Weight: ${pokemonData.height}m, ${pokemonData.weight}kg</span>
          <p class="description">${description}</p>
        </section>
      `;

      pokemonContainer.innerHTML += html;
    } catch (error) {
      console.log(`Error fetching data for Pokemon ID ${randomPokemonId}:`, error);
    }
  }
}

populatePokemonData();
