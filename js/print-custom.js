class manipulateImages extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  // for column safety
  afterParsed(content) {
    content.querySelectorAll("h2").forEach((h2) => {
      h2.style.breakAfter = "avoid";
    });
  }

  async finalizePage(pageFragment, oldpage) {
    // if there is an element you’re looking for
    if (pageFragment.querySelector(".pokemon")) {

      pageFragment.querySelectorAll(".pokemon h2").forEach((el, index) => {
        el.insertAdjacentHTML("afterbegin", `<span class="count">${index + 1}</span>`);
      });

      // create an array that contains all images
      [...pageFragment.querySelectorAll(".pokemon img")].map((el) => {
        if (el.closest(".shiny")) el.classList.add("imgshiny");
      });

      // the spread operator is genius
      let imageList = [...pageFragment.querySelectorAll(".pokemon img")];

      // create a new page
      //
      let page = this.chunker.addPage();

      // emulate the beforepage lyout to add the page to the flow pagedjs is waiting for
      await this.chunker.hooks.beforePageLayout.trigger(
        page,
        undefined,
        undefined,
        this.chunker
      );
      this.emit("page", page);

      // create the wrapper
      const wrapper = document.createElement("div");
      wrapper.classList.add(`pokemon-${imageList.length}`);
      wrapper.classList.add(`pokewrap`);

      // running head get the  first title of the left page?
      wrapper.innerHTML = `<h2 style="display: none">${oldpage.element.querySelector("h2").innerHTML
        }</h2>`;

      // fill the wrapper
      imageList.forEach((image, index) => {
        wrapper.insertAdjacentHTML(
          "beforeend",
          `<figure id="pok${index}" class="${image.className
          }"><span class="number">${index + 1}</span>${image.outerHTML
          }</figure>`
        );
      });

      //import the wrapper on the page
      page.element
        .querySelector(".pagedjs_page_content")
        .insertAdjacentElement("beforeend", wrapper);

      // emulate the finalize page to add the running boxes
      await this.chunker.hooks.afterPageLayout.trigger(
        page.element,
        page,
        undefined,
        this.chunker
      );
      await this.chunker.hooks.finalizePage.trigger(
        page.element,
        page,
        undefined,
        this.chunker
      );
      this.chunker.emit("renderedPage", page);
    }
  }
}
Paged.registerHandlers(manipulateImages);
